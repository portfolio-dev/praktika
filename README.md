## Проект экрана(страницы) VueJS, html, css

Как установить:

1. Склонировать репозиторий **praktika.git**: `git clone https://bitbucket.org/businesstep/praktika.git`
2. Перейти в директорию проекта(для windows): `cd praktika`
3. Установить npm зависимости, выполнив команду: `npm install`

Как запустить:

1. Скомпилировать:

- `npm run dev`	- для компиляции в режиме разработки 
или
- `npm run prod` 	- для компиляции в режиме прода
или
- `npm run watch` - для отслеживания изменений и автоматической перекомпиляции

2. Запустить:

- `npm run start` - для локального запуска на webpack-dev-server
или 
- `php -S localhost:8000 -t ./public` - для локального запуска (встроенный php-сервер)