import HomeComponent from '@/components/Home.vue';

const routes = [
    {
        name: 'home',
        path: '/',
        component: HomeComponent
    },
    {
        name: 'statistics',
        path: '/statistics',
        component: HomeComponent
    },
    {
        name: 'options',
        path: '/options',
        component: HomeComponent
    }
];

export default routes;