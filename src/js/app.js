import * as $ from 'jquery';

import '@css/style.css';
import '@scss/main.scss';

import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import routes from '@js/routes.js';
import App from '@js/App.vue';

const router = new VueRouter({mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({router}, App)).$mount('#app');